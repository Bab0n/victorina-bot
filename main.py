import sqlite3, config, hashlib, time, win32gui, win32con
import win32com.client
import keyboard as kb

chat_path = config.chat_path


def start_message():
    print("""
     █████  ██    ██ ████████  █████        ██    ██ ██  █████  ████████  █████  ██████  ██ ███   ██  █████
    ██   ██ ██    ██    ██    ██   ██       ██    ██ ██ ██   ██    ██    ██   ██ ██   ██ ██ ████  ██ ██   ██
    ███████ ██    ██    ██    ██   ██ █████  ██  ██  ██ ██         ██    ██   ██ ██████  ██ ██ ██ ██ ███████
    ██   ██ ██    ██    ██    ██   ██         ████   ██ ██   ██    ██    ██   ██ ██   ██ ██ ██  ████ ██   ██
    ██   ██  ██████     ██     █████           ██    ██  █████     ██     █████  ██   ██ ██ ██   ███ ██   ██

                                   ██████   █████  ██████   █████  ███   ██ 
                                   ██   ██ ██   ██ ██   ██ ██   ██ ████  ██ 
                                   ██████  ███████ ██████  ██   ██ ██ ██ ██ 
                                   ██   ██ ██   ██ ██   ██ ██   ██ ██  ████ 
                                   ██████  ██   ██ ██████   █████  ██   ███  
      """)


def chat_say(message='/home'):
    kb.send('T')
    time.sleep(config.chat_cooldown)
    kb.write(message)
    kb.send('Enter')
    return


def getSHA256():
    global chat_path
    h = hashlib.sha256()
    b = bytearray(128*1024)
    mv = memoryview(b)
    with open(chat_path, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()
def readFile():
    global chat_path
    with open(chat_path) as f: lines = f.readlines()
    _s = ""
    for ele in lines: _s += ele
    return _s


class Window:
    server_title = config.wind_name
    hwnd = win32gui.FindWindow(None, server_title)
    window_minimazed = False
    def __init__(self) -> None:
        self.update_state()
    def open_window(self):
        shell = win32com.client.Dispatch("WScript.Shell")
        shell.SendKeys('%')
        win32gui.ShowWindow(self.hwnd, win32con.SW_NORMAL)
        win32gui.ShowWindow(self.hwnd, win32con.SWP_SHOWWINDOW)
        win32gui.SetForegroundWindow(self.hwnd)
        self.update_state()
    def minimaze_window(self):
        win32gui.ShowWindow(self.hwnd, win32con.SW_FORCEMINIMIZE)
        self.update_state()
    def update_state(self):
        if win32gui.GetWindowText(win32gui.GetForegroundWindow()) == self.server_title:
            self.window_minimazed = False
        else:
            self.window_minimazed = True

_w = Window()
def answer_give(message, _t, delay):
    def get_ans(_s):
        db = sqlite3.connect('answers.db')
        sql = db.cursor()
        sql.execute(f"SELECT answer FROM ans WHERE quest == '{_s[52:]}'")
        if sql.fetchone() is None:
            return None
        else:
            return sql.execute(f"SELECT answer FROM ans WHERE quest == '{_s[52:]}'").fetchone()


    ans = get_ans(message)
    if ans != None:
        print(f'> Ответ на вопрос получен: "{ans[0]}"')
        print('> Выжидание антибот системы...')
        while time.time() - _t < delay:
            time.sleep(0.001)
        print('> Ответ на вопрос отправляется в чат')
        chat_say(ans[0])
        time.sleep(1)
    else:
        print('> Вопроса нет в базе')
        

_sha256 = getSHA256()
_text = readFile()
def chat_update_check():
    global _sha256, _text
    if getSHA256() != _sha256:
        _new, _sha256, _text = readFile().replace(_text, ""), getSHA256(), readFile()
        messages, _w, _old, timeout = _new.split('\n'), Window(), Window().window_minimazed, config.cooldown

        if len(messages) > 5: return
        
        for i in messages:
            if "[Client thread/INFO]: [CHAT] [Викторина]" in i and not "победил" in i:
                print(f'{i[:10]}:\n> Появился вопрос викторины: "{i[52:]}"')
                if _w.window_minimazed:
                    print('> Происходит открытие окна')
                    _w.open_window()
                    timeout -= config.delay_to_open_window
                time.sleep(0.05)
                answer_give(i, time.time(), timeout)
        
        if _old:
            _w.minimaze_window()
    return

if __name__ == "__main__":
    start_message()
    while True:
        chat_update_check()
        time.sleep(0.05)
